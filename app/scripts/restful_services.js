export async function getPokemonRequest() {
    var response = await fetch('https://pokeapi.co/api/v2/pokemon/?limit=151')
    var data = await response.json()

    var i
    var value = ""
    for(i in data.results)
        value += data.results[i].name + "/"
  
    return value;
  }

export async function postLoginRequest(user, password) {
    var loginBody = {
        username : user,
        password : password
    }
    var response = await fetch('https://www.gutrade.io/GeoV3Desarrollo/Services/MobileService.svc/LoginWithCredentials', {
        method: 'POST',
        body: JSON.stringify(loginBody),
        headers: {
            'Content-type': 'application/json; charset=UTF-8'
        }
    })
    var data = await response.json()

    var responseLogin = {
        personOfInterestFullName : data.PersonOfInterestFullName,
        personOfInterestIdentifier : data.PersonOfInterestIdentifier,
        lastMarkPointOfInterestName : data.LastMarkPointOfInterestName,
        lastMarkType : data.LastMarkType,
        lastMarkTypeDescription : data.LastMarkTypeDescription,
        LastMarkDate : data.LastMarkDate
    }
    
 return responseLogin
}